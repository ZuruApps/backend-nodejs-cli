import * as fs from "fs";

export const bare = () => {
  fs.mkdirSync("./src");

  fs.mkdirSync("./src/@types");
  fs.mkdirSync("./src/@types/express");

  fs.mkdirSync("./src/api/features", { recursive: true });
  fs.mkdirSync("./src/api/middleware/validations", { recursive: true });
  fs.mkdirSync("./src/api/utils");

  fs.mkdirSync("./src/config");

  fs.mkdirSync("./src/db");

  fs.mkdirSync("./src/server/express", { recursive: true });

  fs.mkdirSync("./src/setups");

  fs.mkdirSync("./src/types");
};