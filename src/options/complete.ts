import * as fs from "fs";

export const complete = () => {

  /*
    DOWNLOADING DEPENDENCIES
  */
  


  /*
    CREATING DIRECTORIES AND RELATED FILES
  */

  fs.mkdirSync("./src");

  fs.mkdirSync("./src/@types");
  fs.mkdirSync("./src/@types/express");
  fs.writeFile(
    "./src/@types/express/index.d.ts",
    `declare namespace Express {
  export interface Request {
    // pagination related
    page?: string;
    pageSize?: string;

    // admin related
    isAdminRequest: boolean;

    // auth/user related
    userDataRes: {
      id: number;
      isAdmin: boolean;
    };
  }
}`
    ,
    (error) => {
      if (error) {
        // this.error(error);
        console.error(error);
      }
    });

  // auth feature
  fs.mkdirSync("./src/api/features/auth/controllers", { recursive: true });
  fs.writeFile("./src/api/features/auth/controllers/AuthController.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.mkdirSync("./src/api/features/auth/routes", { recursive: true });
  fs.writeFile("./src/api/features/auth/routes/AuthRoutes.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.mkdirSync("./src/api/features/auth/services", { recursive: true });
  fs.writeFile("./src/api/features/auth/services/AuthService.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // users feature
  fs.mkdirSync("./src/api/features/users/controllers", { recursive: true });
  fs.writeFile("./src/api/features/users/controllers/UserController.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.mkdirSync("./src/api/features/users/routes", { recursive: true });
  fs.writeFile("./src/api/features/users/routes/UserRoutes.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.mkdirSync("./src/api/features/users/services", { recursive: true });
  fs.writeFile("./src/api/features/users/services/UserService.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.mkdirSync("./src/api/middleware/validations", { recursive: true });

  fs.mkdirSync("./src/api/utils", { recursive: true });
  fs.writeFile("./src/api/utils/Constants.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/Filter.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/ApiResponse.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/GenerateAccessToken.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/GeneratePasswordResetToken.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/ImageUpload.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/Pagination.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/api/utils/Sort.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });


  fs.mkdirSync("./src/config/email", { recursive: true });
  fs.writeFile("./src/config/email/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.mkdirSync("./src/config/sequelize", { recursive: true });
  fs.writeFile("./src/config/sequelize/database.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.mkdirSync("./src/db/migrations", { recursive: true });
  fs.mkdirSync("./src/db/models", { recursive: true });
  fs.writeFile("./src/db/models/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.mkdirSync("./src/server/express", { recursive: true });
  fs.writeFile("./src/server/express/app.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/server/express/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/server/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.mkdirSync("./src/setups");
  fs.writeFile("./src/setups/googleCloudStorage.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./src/setups/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  fs.mkdirSync("./src/types");
  fs.writeFile("./src/types/index.ts", "", (error) => {
    if (error) {
      console.log(error);
    }
  });


  /*
    CREATING ROOT DIR FILES (../src path)
  */

  // environment variable files
  fs.writeFile("./.env.development", "", (error) => {
    if (error) {
      console.log(error);
    }
  });
  fs.writeFile("./.env.test", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // gitgnore
  fs.writeFile(
    "./.gitignore",
    `node_modules/
`,
    (error) => {
      if (error) {
        console.log(error);
      }
    });

  // gitlab CI/CD
  fs.writeFile("./.gitlab-ci.yml", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // sequelize root config
  fs.writeFile("./.sequelizerc", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // jest config file
  fs.writeFile("./jest.config.js", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // nodemon
  fs.writeFile(
    "./nodemon.json",
    `{
  "restartable": "rs",
  "ignore": ["node_modules/**/node_modules"],
  "delay": "1000"
}`,
    (error) => {
      if (error) {
        console.log(error);
      }
    });

  // heroku Procfile
  fs.writeFile(
    "./Procfile",
    `release: sequelize-cli db:migrate
web: node build/server/index.js`,
    (error) => {
      if (error) {
        console.log(error);
      }
    });

  // README file
  fs.writeFile("./README.md", "", (error) => {
    if (error) {
      console.log(error);
    }
  });

  // TSConfig file
  fs.writeFile("./tsconfig.json", "", (error) => {
    if (error) {
      console.log(error);
    }
  });


};


