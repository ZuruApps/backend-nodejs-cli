import { Command, flags } from "@oclif/command";
import { bare } from "../options/bare";
import { complete } from "../options/complete";

/**
 * creates Express initial skeleton
 */
export class GenProject extends Command {
  static description = 'creates a folder'

  static flags = {
    complete: flags.boolean({ char: "c", description: "generates complete project, with file templates included" })   
  }

  async run() {
    const { flags } = this.parse(GenProject);

    if (flags.complete) { // generate complete project
      complete();
    } else { // generate bare minimum project
      bare();
    }

    this.log(`project generated successfully`);
  }
}