znodecli
========

Zuru CLI tool for scaffolding backend (nodejs + express) REST API projects

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/znodecli.svg)](https://npmjs.org/package/znodecli)
[![Downloads/week](https://img.shields.io/npm/dw/znodecli.svg)](https://npmjs.org/package/znodecli)
[![License](https://img.shields.io/npm/l/znodecli.svg)](https://gitlab.com/ZuruApps/backend-nodejs-cli/-/blob/main/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g znodecli
$ znodecli COMMAND
running command...
$ znodecli (-v|--version|version)
znodecli/0.0.2 win32-x64 node-v14.14.0
$ znodecli --help [COMMAND]
USAGE
  $ znodecli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`znodecli genProject`](#znodecli-genproject)
* [`znodecli hello [FILE]`](#znodecli-hello-file)
* [`znodecli help [COMMAND]`](#znodecli-help-command)

## `znodecli genProject`

creates a folder

```
USAGE
  $ znodecli genProject

OPTIONS
  -c, --complete  generates complete project, with file templates included
```

_See code: [src/commands/genProject.ts](https://gitlab.com/ZuruApps/backend-nodejs-cli/blob/v0.0.2/src/commands/genProject.ts)_

## `znodecli hello [FILE]`

describe the command here

```
USAGE
  $ znodecli hello [FILE]

OPTIONS
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print

EXAMPLE
  $ znodecli hello
  hello world from ./src/hello.ts!
```

_See code: [src/commands/hello.ts](https://gitlab.com/ZuruApps/backend-nodejs-cli/blob/v0.0.2/src/commands/hello.ts)_

## `znodecli help [COMMAND]`

display help for znodecli

```
USAGE
  $ znodecli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.2/src/commands/help.ts)_
<!-- commandsstop -->
